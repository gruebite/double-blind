# Introduction

Double Blind is a 2019 7DRL entry. This game is designed around the idea of "local versus with imperfect information." If you can imagine the playing Go Fish on a single screen everyone sees, you can understand the challenge. In Double Blind, player input is the hidden information.

In Double Blind, you play as a notorious Rogue adept at navigating the dark. When in the dark, characters are completely hidden on the screen. Players have to remember where they are based on what actions / movements they take. The Rogue has several tools at their disposal when in the dark like throwing knives and laying decoys. The goal is to find enemies in the dark, before they find you.

# Gameplay

Game play is centered around hiding and seeking in the dark. The first player to find and kill the other wins.

## Rooms

A room is where the game takes place. Rooms are somewhat procedurally generated. They will be square with random obstacles thrown about depending on the room type. When entering a room, you will be able to see everything for a few seconds (?).

### Type: Generic Room

Currently this is the only room type.

#### Contents

- **Wood Walls**, cannot push
- **Stone Walls**, cannot push
- **Stone Pillars**, cannot push
- **Bookshelf**, cannot push
- **Desk**, can push, provides cover, can stumble
- **Wooden Table**, cannot push, cannot see in the dark, hides when under
- **Chair**, can stumble over
- **Torch**, lights area, can be distinqished
- **Wood Floor**, quiet noise
- **Stone Floor**, silent
- **Wooden Door**, will be locked until a key is found?

### Type: Social?

### Type: Bedroom?

### Type: Celler?

## Controls

The Rogue is controlled with four arrows: `^``>``v``<`, and two buttons: `A`, `B`. The Rogue also has a currently selected action, which defaults to "Move". When using a directional key you will spend an action point (AP), and use it in that direction. Some actions don't require a direction, so any direction will work.

In order to change your selection action, you open the action menu with `A`. From there you will be presented with a menu that requires two arrow key inputs.

- `^``>``v``<` use current action in direction
- `A` open action select (see below)
- `B` interact in direction?

### Actions

Actions have feedback in the form of success/failure indications. Rogues also have 2 or more AP per turn and will see their AP drop. Some actions create noise, this noise will be displayed as a **noise animation** in an area with a random center on the point of origin. Loud noises will have a smaller area, quieter noises will have a larger area.

#### Noise Levels

1. Quiet noise: 7 area
1. Normal noise: 5 area
2. Loud noise: 3 area

Noises are shown graphically, in addition to an actual sound, to indicate where they are. They are fuzzy, but can give some information. A noise will have a point of origin, the noise area will be placed randomly so that part of the area is over the origin. An example with a loud noise:

X = origin, * = noise

```
.*.
*X*
.*.

or

.*.
X**
.*.

or

.X.
***
.*.

etc.
```

Stumbling over an object is a medium noise.

#### Action List

`A` will bring you back to the map.

`B` will ???

- `^^` Move
- `^>` Move object silently
- `^v` Dash -- 2 squares makes quiet noise
- `^<` Prone -- immune to throws, only slashes work

- `>>` Throw knife -- makes loud noise if misses
- `>v` Slash -- destroys traps/decoys, 3 nearby squares in direction
- `><` Lay trap -- can only have 1 active trap, makes loud noise and removes an action point from this turn or next
- `>^` ???

- `vv` Sense area -- success/failure if in direction of Rogue (cone or...?)
- `v<` Sense distance -- ^ is 8+, v is 0-3, <> is 4-7
- `v^` Sense trap -- slash check, first 3 squares in direction
- `v>` ???

- `<<` Light match -- can see surrounding and you for a second
- `<^` Lay decoy -- can only have 1 active decoy, sense falsely detects this
- `<>` Flash bomb -- throw in direction, lights up area 3 squares away
- `<v` ???

## The Rogue

**The Rogue** is notorious. However, not much is known of the Rogue in the beginning, but will be told throughout the story.

# Versus

This is the focus point. In this version, a single room is generated and 2 players enter the field. There are no doors. All actions are unlocked.

# Story

You are trying to steal a priceless amulet from The Twisted Mansion.

## The Twisted Mansion

The Twisted Mansion is a series of rooms linked together by doors. Each room is presented separately and is procedurally generated. No room is saved, and going back the way you came regenerates the room. You will find clues on which doors to go through.

## Enemies

### Traps

Traps can be placed around the mansion. They cannot be seen and can make a loud noise when triggered.

### Dummies

**Dummies** are decoys in the dark. They are harmless but can be stumbled over like a chair. Attacking them makes a medium noise.

### Guards

**Guards** wander aimlessly. They around aimlessly around the last noise area in the dark for a few turns before looking elsewhere. They will immediately run for a lit Rogue.

### Dogs

**Dogs** will wander slightly weighted towards the Rogue, regardlness of quietness. They are less susceptible to noises.

# Tutorial

Tutorial is just some pre-generated levels leading up to the story that introduce the basic concepts.

1. Lit room, learning movement and dashing.
2. Mostly lit room, learning darkness and pushing a chair.
3. Dark room, chair blocking door.
4. Dark room, dummy that must be found and attacked using senses.
5. Torch, (how to take out torches?)
