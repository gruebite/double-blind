

SRC_DIR=src
BIN_DIR=bin

LUA=code.p8
GFX=art-8.p8
GFF=art-8.p8
MAP=art-8.p8
SFX=sfx.p8
MUSIC=sfx.p8
OUT=double-blind.p8.png

.PHONY: all dist clean

$(OUT):
	p8tool build $(OUT) \
		--lua $(SRC_DIR)/$(LUA) \
		--gfx $(SRC_DIR)/$(GFX) \
		--map $(SRC_DIR)/$(MAP) \
		--sfx $(SRC_DIR)/$(SFX) \
		--music $(SRC_DIR)/$(MUSIC)
	#p8tool luafmt $(OUT)

dist: $(BIN_DIR)/index.html $(BIN_DIR)/index.js
	zip $(BIN_DIR)/double-blind.zip $(BIN_DIR)/index.html $(BIN_DIR)/index.js
	./butler push $(BIN_DIR)/double-blind.zip gruebite/double-blind:double-blind-stable-universal

clean:
	rm $(OUT)
