pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
-- double blind
-- by gruebite
-- art by vaporstack
-- sound by gruber

-- fix bug with carry over moves across versus

function _init()
 menuitem(1, "toggle hide", function()
  if (_p) _p.nohide = not _p.nohide
 end)
 menuitem(2, "end turn", function()
  if (_p) _p.mp = 0
 end)
 palt(0,false)
 palt(14,true)
	menu_init()
end

-->8
-- constants,utilities

local _enemy_rates={
	easy={
		{guards=1,dogs=0,dummies=0},
		{guards=0,dogs=0,dummies=1},
		{guards=0,dogs=0,dummies=1}
	},
	medium={
  {guards=0,dogs=1,dummies=1},
		{guards=0,dogs=1,dummies=0},
  {guards=1,dogs=0,dummies=1},
		{guards=0,dogs=0,dummies=2}
	},
	hard={
		{guards=0,dogs=2,dummies=0},
		{guards=1,dogs=1,dummies=0},
  {guards=2,dogs=0,dummies=1},
		{guards=1,dogs=0,dummies=2},
		{guards=0,dogs=1,dummies=1}
	}
}

local _keyboard=stat(102)!=0 and stat(102)!="www.lexaloffle.com" and stat(102)!="www.playpico.com"
local _obtn=_keyboard and "z" or "🅾️"
local _xbtn=_keyboard and "x" or "❎"

local _default_mp=3

local _default_info_time=90

local _mw=16
-- 1 for top bar,1 for bottom
local _mh=14

local _digits={"0","1","2","3","4","5","6","7","8","9"}

local _deltas={
	{-1,-1},
	{-1,0},
	{-1,1},
	{ 0,-1},
	{ 0,0},
	{ 0,1},
	{ 1,-1},
	{ 1,0},
	{ 1,1}
}

local _sounds={
	loud=0,
	noisy=1,
	normal=2,
	quiet=3,
	hushed=4
}

local _texts={
	"rodney's spellbook. cool.",
	"this mansion has many secrets...",
	"every door is an experiment.",
	"i should read more.",
	"some doors see, some do not.",
	"one door likes, the other hates.",
	"four directions, which way?",
	"past doors knows the way.",
 "guerilla tactics against grues."
}

function lerp(a,b,t)
 return a + (b - a) * t
end

function rep(s,n)
 local r=""
 for i=1,n do
  r=r .. s
 end
 return r
end

function round(n)
 if n < 0 then
  return flr(n+0.5)
 else
  return -flr(-n+0.5)
 end
end

function dice(s,n)
 r=0
 for i=1,n or 1 do
  r += flr(rnd(s)+1)
 end
 return r
end

function choose(list)
 return list[flr(rnd(#list))+1]
end

function rnd_room()
  return
   flr(rnd(_mw-4)+2),
   flr(rnd(_mh-4)+2)
end

function rnd_wall()
  local side=flr(rnd(4))
  -- left,top,right,bottom
  if side == 0 then
   return
    1,
    flr(rnd(_mh-4)+2)
  elseif side == 1 then
   return
    flr(rnd(_mw-4)+2),
    1
  elseif side == 2 then
   return
    _mw-2,
    flr(rnd(_mh-4)+2)
  elseif side == 3 then
   return
    flr(rnd(_mw-4)+2),
    _mh-2
  end
end

function rnddig() return _digits[dice(10)] end

-- excludes walls
function in_room(x,y)
	return x >= 1 and x < _mw-1 and
								y >= 1 and y < _mw-1
end

-- includes walls
function in_map(x,y)
	return x >= 0 and x < _mw and
								y >= 0 and y < _mh
end

function line_travel(x,y,dx,dy,cb)
	x,y=x+dx,y+dy
	while in_map(x,y) do
		if not cb(x,y) then
			return
		end
		x,y=x+dx,y+dy
	end
end

function slash_travel(x,y,dx,dy,cb)
	for d=-1,1 do
		local i,j
		if dx == 0 then
			i=d
		 j=dy
		else
			i=dx
		 j=d
		end
		if not cb(x+i,y+j) then
			return
		end
	end
end

function sneak(e,dx,dy)
	e.prone=nil
	local tx=e.x+dx
	local ty=e.y+dy
	local what=mfeel(tx,ty)
	if not what then
		e.x=tx
		e.y=ty
		mvs()
	else
		if eis(what,"trap") then
			what.lifetime=0
			boom(tx,ty,"loud","sound",e)
			e.x=tx
			e.y=ty
   sfx(52)
		elseif eis(what,"door") then
			-- do nothing
			return what
		elseif eis(what,"interact") then
			return what
		else
		 boom(e.x,e.y,"normal","sound",e)
   sfx(48)
		end
		mvf(43)
	end
	-- only function to return interaction
	return what
end

function dash(e,dx,dy)
	e.prone=nil
	local tx=e.x+dx
	local ty=e.y+dy
	local what=mfeel(tx,ty)
	if not what then
		e.x=tx
		e.y=ty
	else
		if eis(what,"trap") then
			what.lifetime=0
			boom(tx,ty,"loud","sound",e)
			e.x=tx
			e.y=ty
			e.energy = 0
   sfx(52)
		else
		 boom(tx,ty,"noisy","sound",e)
   sfx(48)
		end
		mvf(43)
  return
	end
	--
	tx=e.x+dx
	ty=e.y+dy
	what=mfeel(tx,ty)
	if not what then
		e.x=tx
		e.y=ty
		boom(e.x,e.y,"quiet","sound",e)
		mvs()
	else
		if eis(e,"trap") then
			what.lifetime=0
			boom(tx,ty,"loud","sound",e)
			e.x=tx
			e.y=ty
			e.energy = 0
   sfx(52)
		else
		 boom(e.x,e.y,"normal","sound",e)
   sfx(48)
		end
		mvf(43)
	end
end

function feign(e,dx,dy)
	local px,py=e.x,e.y
	local r = 4
	line_travel(e.x+dx,e.y+dy,dx,dy,function(x,y)
		local what=mfeel(x,y)
		if type(what) == "number" or r == 0 then
			boom(px,py,"quiet","sound",e)
   mvf(48)
			return false
		end
		r -= 1
		px,py=x,y
		return true
	end)
 mvs()
end

function prone(e,dx,dy)
	if e.prone then
		mvf()
	else
	 e.prone=true
	 mvs()
	end
end

function throw_knife(e,dx,dy)
	local px,py=e.x,e.y
	local delay=1

	line_travel(e.x+dx,e.y+dy,dx,dy,function(x,y)
		local what=mfeel(x,y)
		if type(what) == "number" then
			boom(px,py,"noisy","sound",e)
			mvf(56)
			return false
		elseif what and not eis(what,"prone") then
			if attack(what) then
				mvs(43)
			else
				boom(px,py,"noisy","sound",e)
			 mvf(55)
			end
			return false
		end
		knife(x,y,delay)
		delay += 1
		px,py=x,y
		return true
	end)
end

function slash(e,dx,dy)
	local delay=1
	local success=false
	slash_travel(e.x,e.y,dx,dy,function(x,y)
		local what=mfeel(x,y)
		if type(what) == "number" then
			boom(e.x,e.y,"noisy","sound",e)
   sfx(56)
		elseif eis(what,"x") then
			if attack(what) then
				success=true
			else
				boom(e.x,e.y,"noisy","sound",e)
    sfx(55)
			end
		end
		knife(x,y,delay,true)
		delay += 2
		return true
	end)
	if success then
		mvs(43)
	else
	 mvf()
	end
end

function lay_trap(e,dx,dy)
	local x,y=e.x+dx,e.y+dy
	if mfeel(x,y) then
		mvf()
		return
	end
	if e.trap then
		e.trap.lifetime=0
	end
	e.trap=eadd {
		x=x,y=y,
		trap=true,
  prone=true,
		anim=animate(63,63,0)
	}
	mvs()
end

function sense_area(e,dx,dy)
	if dx == 0 then
		for y=e.y+dy,dy == -1 and 1 or _mh-2,dy do
			for x=0,_mw-1 do
				local e=eget(x,y)
				if e and e.hp then
					mvs()
					return
				end
			end
		end
	elseif dy == 0 then
		for x=e.x+dx,dx == -1 and 1 or _mw-2,dx do
			for y=0,_mh-1 do
				local e=eget(x,y)
				if e and e.hp then
					mvs()
					return
				end
			end
		end
	end
	mvf()
end

function sense_trap(e,dx,dy)
	local success=false
	slash_travel(e.x,e.y,dx,dy,function(x,y)
		local what=mfeel(x,y)
		if eis(what,"trap") then
			success=true
		end
		return true
	end)
	if success then
		mvs()
	else
	 mvf()
	end
end

function lay_decoy(e,dx,dy)
	local x,y=e.x+dx,e.y+dy
	if mfeel(x,y) then
		mvf()
		return
	end
	if e.decoy then
		e.decoy.lifetime=0
	end
	e.decoy=edummy(x,y)
	mvs()
end

function light_match(e,dx,dy)
	for i=-1,1 do
		for j=-1,1 do
			if i == 0 or j == 0 then
				mlight(e.x+i,e.y+j,true)
			end
		end
	end
	e.last_seen={x=e.x,y=e.y}
	eadd {
		x=e.x+dx,
		y=e.y+dy,
		lifetime=30,-- 1 sec
		anim=function(self)
			if self.lifetime == 1 then
				mfov()
				return
			end
		end
	}
	mvs(50)
end

function push_obj(e,dx,dy)
	e.prone=nil
	local tx,ty=e.x+dx,e.y+dy
	local what=mfeel(tx,ty)
	if type(what) == "number" then
		mvf(43)
		return
	elseif eis(what,"furn") then
		sneak(what,dx,dy)
	else
	 mvf(43)
	end
end

function flash_bomb(e,dx,dy)
	line_travel(e.x,e.y,dx,dy,function(x,y)
		local what=mfeel(x,y)
		if what then
			if eis(what,"prone") then
			 -- skip prone entities
				return true
			end
			if eis(what,"furn") then
				local nx,ny=what.x,what.y
				what.x=-100
				what.y=-100
				what.lifetime=0
				etorch(nx,ny)
    sfx(50)
			else
				ebomb(x,y)
    sfx(48)
			end
			mfov()
			if mlit(e.x,e.y) then
				e.last_seen={x=e.x,y=e.y}
			end
			return false
		end
		return true
	end)
	mvs(43)
end

local _moves={
	false,{name="dash",f=dash},{name="feign",f=feign},{name="prone",f=prone},
	{name="throw knife",f=throw_knife},false,{name="slash",f=slash},{name="lay trap",f=lay_trap},
	{name="sense area",f=sense_area},{name="sense trap",f=sense_trap},false,{name="lay decoy",f=lay_decoy},
	{name="light match",f=light_match},{name="push obj",f=push_obj},{name="flash bomb",f=flash_bomb},false
}

function disable_moves(bool)
	for m in all(_moves) do
		if (m) m.disabled = bool
	end
end

-->8
-- ui,aux gfx

function animate(n,m,t,col)
 local c=t
 local i=n
 return function(self)
  if c == 0 then
   i += 1
   c=t
  end
  if i > m then i=n end
  c -= 1
  local x,y=self.x*8,self.y*8
  if col then pal(7,col) end
  spr(i,x,y+8)
  pal(7,7)
 end
end

_al=' !"#%\'()*+,-./0123456789:;<=>?abcdefghijklmnopqrstuvwxyz[]^_{~}'
function chartonum(c)
  local i
  for i=1,#_al do
    if sub(_al,i,i) == c then
      return i+194
    end
  end
  return 0
end

function split(str)
 local l=#str
 local r={}
 for i=1,l do
  add(r,sub(str,i,i))
 end
 return r
end

function font(w,x,y)
  for i=1,#w do
    x += 8
    c = chartonum(sub(w,i,i))
    spr(c,x,y)
  end
end

local balls = {
 animate(138,140,12,5),
 animate(138,140,12,7)
}

function energy(n,x,y,p)
 for i=1,n do
  balls[p and 1 or 2]({x=x+(i-1)-(n/2),y=y})
 end
end

function printc(str,x,y,col,sz)
	print(str,x-(#str/2)*(sz or 4),y,col or 7)
end

local bt={0,0,0,0,0}
function btng(n,p,f)
	f=f or 5
 local b=btn(n,p)
 if btnp(n,p) and bt[n] == 0 then
  bt[n] += 1
 elseif not b and bt[n] > 0 then
  local _=bt[n]
  bt[n]=0
  if (_ <= f) return "p"
 elseif b and bt[n] > 0 then
  bt[n] += 1
  if (bt[n] > f) return "h"
 end
 return b
end

uis={}

local info_msg=nil

function uiadd(ui)
	add(uis,ui)
	ui.x=flr(ui.x)
	ui.y=flr(ui.y)
	return ui
end

function uidraw()
	for ui in all(uis) do
		if ui.anim and (mlit(ui.x,ui.y) or ui.nohide) then
			ui:anim()
		end
	end
end

function uiupdate()
	for ui in all(uis) do
		if ui.lifetime then
			if ui.lifetime == 0 then
				del(uis,ui)
			else
				ui.lifetime -= 1
			end
		end
	end
end

function uiinfo(msg,life)
	if info_msg then
		del(uis,info_msg)
	end
 sfx(21,-1,0,min(#msg/2,32))
	info_msg=uiadd {
		x=0,y=120,
		lifetime=life or _default_info_time,
		nohide=true,
		scroll=0,
		msg=msg,
		anim=function(self)
			local c
			if not self.lifetimer or self.lifetime > 15 then
				c=7
			else
			 c=5
			end
			self.scroll += 2
			local m=sub(msg,1,self.scroll)
			print(m,self.x,self.y,c)
		end
	}
	return info_msg
end

function box(col,kind,inner)
	local o = inner and 2 or 0
	return uiadd {
		x=0,y=0,
		lifetime=kind == "shaded" and 1 or 3,
		nohide=true,
		boxkind=kind,
		anim=function(_)
			rectfill(0+o,8,2+o,120,col)
   rectfill(127-o,8,125-o,120,col)
		end
	}
end

function uisuccess()
	return box(11,"success")
end

function uifail()
	return box(8,"failure")
end

function uinorm()
	return box(7,"normal")
end

function uishade()
	return box(5,"shaded",true)
end

function mvs(s)
 sfx(s or 60)
 uisuccess()
end
function mvf(s)
 sfx(s or 59)
 uifail()
end

function knife(x,y,delay,blade)
	return eadd {
		x=x,y=y,
		lifetime=3+delay,
		-- we want to hide this in the
		-- dark,so nohide=false
		anim=(function()
   local a
   if not blade then
    a = animate(166,169,2)
   else
    a = animate(186,190,2)
   end
   return function(self)
 			if self.lifetime <= 3 then
 				a(self)
 			end
   end
		end)()
	}
end

function blood(x,y)
	return uiadd {
		x=x,
		y=y,
		nohide=true,
		lifetime=21,
		anim=animate(161,163,7,8)
	}
end

function sound(x,y)
	return uiadd {
		x=x,
		y=y,
		nohide=true,
		lifetime=16,
		anim=animate(181,184,4,10)
	}
end

-- can change color for blood
function boom(x,y,size,kind,player)
	local rads=_sounds[size]
	local cx,cy=0,0
	local tries=10
	while tries > 0 do
		tries -= 1
		local angle=rnd(1)
		local tx=round(cos(angle)*rnd(rads))
		local ty=round(sin(angle)*rnd(rads))
		if tx*tx+ty*ty <= rads*rads then
			cx,cy=tx,ty
			break
		end
	end
	for dx=cx-rads,cx+rads do
		for dy=cy-rads,cy+rads do
			if (dx-cx)^2+(dy-cy)^2 <= rads^2 then
				local x,y=x+dx,y+dy
				if player and not msolid(x,y) and in_room(x,y) then
					add(player.last_sounds,{x=x,y=y})
				end
				if kind == "blood" then
				 blood(x,y)
				else
					sound(x,y)
				end
			end
		end
	end
end

local arrows={}
--[[
0,4,8,12
1,2,3,4
--]]
function arrow_combo(inp,items)
	add(arrows,inp)
	if #arrows == 2 then
		local ret=arrows[1]*4 +
														arrows[2]+1
		arrows={}
		if ret <= #items and (not items[ret] or not items[ret].disabled) then
   sfx(22)
			uisuccess()
			return ret
		else
   sfx(23)
			uifail()
			return -1
		end
	end
 sfx(22)
	uinorm()
	return true
end

function arrow_clear()
 arrows = {}
end

function arrow_chars(a, x, y)
	local c={
		142,174,158,190
	}
	spr(c[flr(a/4)+1],x,y)
	spr(c[(a%4)+1],x+8,y)
end

function arrow_menu(title,subtitle,player,items,hide,dx,dy)
	camera(-(dx or 0),-(dy or 0))
	printc(title ,64,0)
	printc(subtitle ,64,8,5)
 color(7)
	printc(rep("◆",2-#arrows),64,16,7,8)
	camera()
	local cmb=0

	if #items <= 8 then
		local y=72-#items/2*8
		for i=1,#items do
			local item=items[i]
			if item and item.name then
				if hide or not item.disabled then
					color(7)
				else
				 color(5)
				end
				arrow_chars(cmb,64-20,y)
				print(item.name,64,y)
			end
			y += 8
			cmb += 1
		end
	else
		-- slightly different from above
		local y=64-#items/4*8
		local x=0
		for i=1,#items do
			local item=items[i]
			if i == 9 then
				x=64
				-- reset
				y=64-#items/4*8
			end
			if item and item.name then
				if hide or not item.disabled then
					color(7)
				else
				 color(5)
				end
				arrow_chars(cmb,x,y)
				-- 20=2.5*8
				print(item.name,x+20,y)
				y += 8
			end
			cmb += 1
			if i % 4 == 0 then
				y += 8
			end
		end
	end

	if btnp(0,player) then
		return arrow_combo(0,items)
	elseif btnp(1,player) then
	 return arrow_combo(1,items)
	elseif btnp(2,player) then
	 return arrow_combo(2,items)
	elseif btnp(3,player) then
	 return arrow_combo(3,items)
	elseif btnp(5,player) then
		-- reset for next open
		arrows={}
		return -1
	end
	return false
end

function arrow_hidden(player,items)
	if btnp(0,player)	then
	 return arrow_combo(0,items)
	elseif btnp(1,player) then
	 return arrow_combo(1,items)
	elseif btnp(2,player) then
	 return arrow_combo(2,items)
	elseif btnp(3,player) then
	 return arrow_combo(3,items)
	end
end

local holdbox = nil
function move_control(plr,moves,cb,note)
	local b=btng(5,plr.player)

	if b == "p" then
		move_select_init(plr,cb,note,moves)
	elseif b == "h" then
		uishade()
		local sel=arrow_hidden(plr.player,moves)
		if sel then
			if type(sel) == "number" and moves[sel] then
				plr.move=moves[sel]
				if note then
					uiinfo("selected '" .. moves[sel].name .. "'!")
				end
			end
		end
		uishade()
	elseif not b then
		arrow_clear()
	end
	return b
end

-->8
-- states: menu,versus,game

function menu_init()
	_draw=menu_draw
	_update=menu_update
 eclear()
 disable_moves(false)
	music(5)
end

function menu_draw()
	cls()

	local sel=arrow_menu(
		"",
		"",0,{
		nil,
		{name="versus"},
		{name="new game"},
		{name="tutorial"},
	},false,0,24)

	font("doub e",24,8)
	font("b ind",48,16)
	spr(193,64,0)
	spr(193,64,8)
	spr(193,64,16)
	spr(193,64,24)

	if sel == 2 then
  music(-1)
		versus_init()
	elseif sel == 3 then
  music(-1)
		game_init()
	elseif sel == 4 then
  music(-1)
		tutorial_init()
	end

	printc("press arrows in sequence",64,120,5)

	uidraw()

end

function menu_update()
	uiupdate()
end

-- **

local tst
function true_sight(t)
 if t then
  tst = t
 else
  if tst and tst > 0 then
   tst -= 1
   if tst == 0 then
    uiinfo("true sight lost!")
    mlight_all(false)
    mfov()
    return true
   else
    if tst % 30 == 0 then
     local s=tst/30
     uiinfo(tostr(s) .. " secs...")
   	end
   end
  else
   return true
  end
 end
end

local ait
function ai_done(t)
 if t then
  ait = t
 else
  if ait and ait > 0 then
   ait -= 1
   if ait == 0 then
    return true
   end
  else
   return true
  end
 end
end

function ai_workleft()
 if (ait) return ait
 return 0
end

-- ***

local transition_cb
local transition_delay

function transition_init(update,draw,delay,cb)
 transition_cb=cb
 transition_delay=delay
 _update=function()
  transition_delay -= 1
  if transition_delay == 0 then
   cb()
  end
  if update then
   update()
  end
 end
 _draw=draw
end

function transition_blackout_init(cb)
 local t = 0
 transition_init(base_update,function()
  for i=0,30 do
   local x,y=rnd(128),rnd(128)
   pset(x,y,7)
  end
  t += 1
  if (t == 1) base_draw()
 end,30,cb)
end

-- ***

function base_draw()
	cls()
	mapdraw(0,0,0,8,_mw,_mh)
	edraw()
	uidraw()
 if ai_workleft() > 0 then
  rectfill(8,7,120,7,7)
  rectfill(lerp(64,7,ai_workleft() / 15),7,64,7,5)
  rectfill(64,7,lerp(64,120,ai_workleft() / 15),7,5)
  if ai_workleft() == 1 then
   rectfill(64,6,64,8,7)
  end
  return false
 end
 return true
end

function base_update()
 eupdate()
	uiupdate()
	return true_sight()
end

function base_single_update(cb, skip)
 if ai_done() then
  local bump = _p:do_rogue()

  if _p.mp == 0 then
   _p.mp=_default_mp
   disable_moves(false)
   eturn()
   if enemies() and not skip then
    ai_done(15)
   end
   -- reset for ai
   _p.last_sounds={}
   if mlit(_p.x,_p.y) then
    _p.last_seen={x=_p.x,y=_p.y}
   end
  end

  if eis(bump,"door") then
   _p.mp=_default_mp
   bump.lifetime=30
   bump.anim=animate(6,7,15)

   sfx(53)
   transition_init(base_update,base_draw,30,function()
    if (cb) cb(bump)
   end)
  end

  if _p.hp <= 0 then
   base_draw()
   transition_blackout_init(function()
    game_over_init("game over","you died! :(",true)
   end)
  end

  return bump
 end
end

-- ***

local turn

function versus_init()
	_draw=versus_draw
	_update=versus_update

	mgenerate()
	mlight_all(true)
	_p=erogue(1,1,0)
	_p2=erogue(_mw-2,_mh-2,1)

	uiinfo("true sight for 5 seconds...",45)
	true_sight(150)
	--music(8)
	turn=_p
end

function versus_draw()
	base_draw()
 print("|",62,2,7)
 energy(_p.mp,4,-1,turn != _p)
	print("p1",0,2,turn == _p and 7 or 5)
 energy(_p2.mp,12,-1,turn != _p2)
	print("p2",120,2,turn == _p2 and 7 or 5)
end

function versus_update()
	if not base_update() then return end

	if move_control(turn,_moves,function()
			_draw=versus_draw
			_update=versus_update
	end) then
		return
	end

	turn:do_rogue()

	if turn.mp == 0 then
  turn.mp = _default_mp
		disable_moves(false)
		if turn == _p then
			turn = _p2
		elseif turn == _p2 then
			turn = _p
		end
	end

 if _p2.hp <= 0 or _p.hp <= 0 then
  mlight_all(true)
  if _p2.hp <= 0 then
   transition_blackout_init(function()
    game_over_init("p1 wins!","")
   end)
  elseif _p.hp <= 0 then
   transition_blackout_init(function()
    game_over_init("p2 wins!","")
   end)
  end
 end
end

-- ***

local guesses=0
local cleared=false

function new_room(diff)
	eclear()
	_p.x,_p.y=rnd_wall()
	eadd(_p)
	mgenerate()
	mpopulate(diff)
	mlight_all(true)
	uiinfo("true sight for 5 secs...",45)
	true_sight(150)
	cleared=false
end

function game_init()
	_draw=game_draw
	_update=game_update

	_p=erogue(1,1,0)
	new_room("easy")
	init_mansion()
	--music(8)
end

function game_update()
	if not base_update() then return end

	if move_control(_p,_moves,function()
			_draw=game_draw
			_update=game_update
	end,true) then
		return
	end

 base_single_update(function(bump)
  if go_thru(bump) then
   guesses += 1

   if guesses == 3 then
    transition_blackout_init(function ()
     game_over_init("you won!","take the amulet back in versus.",true)
    end)
    return
   elseif guesses == 2 then
    new_room("hard")
   elseif guesses == 1 then
    new_room("medium")
   end
   uisuccess()
  else
   guesses = 0
   new_room("easy")
   uifail()
  end
  _update=game_update
  _draw=game_draw
 end)

 if not enemies() and not cleared then
  cleared=true
  reveal_doors()
  make_things_interract()
  uiinfo("cleared!")
 end
end

function game_draw()
	if not base_draw() then return end

 energy(_p.mp,8,-1)
end

-- ***

local startx=_mw/2
local starty=_mh-2

local script

local current_idx=0
local current_script
local blink_timer=6
local blink=true

local game_on

local tutorial_moves

local tutorial_torch
local tutorial_dummy
local tutorial_enemy

function find_move(name)
	for i=1,#_moves do
		if _moves[i] and _moves[i].name == name then
			return i,_moves[i]
		end
	end
end

function add_move(name)
	local i,mv=find_move(name)
	tutorial_moves[i]=mv
end

function next_script()
	if current_script == nil and current_idx != 0 then
		 return
	end
	while true do
		current_idx += 1
		current_script=script[current_idx]
		if current_script == true then
			break
		elseif current_script == nil then
			break
		elseif type(current_script) == "thread" then
			break
		elseif type(current_script) == "string" then
			if type(script[current_idx+1]) == "boolean" then
				uiinfo(current_script).lifetime=nil
			else
			 uiinfo(current_script)
			end
		elseif type(current_script) == "function" then
			current_script()
		end
	end
end

function tutorial_init()
	_update=tutorial_update
	_draw=tutorial_draw

	tutorial_moves={
		false,_moves[2],false,false,
		false,false,false,false,
		false,false,false,false,
		false,false,false,false
	}

	game_on = false
 _p=erogue(_mw/2,_mh-2)

	script	= {
		"welcome to the foyer.",true,
		"sneak around with arrow keys.",false,
		function()
			mgenerate()
			mlight_all(true)
		end,
		cocreate(function()
			local px,py=_p.x,_p.y
			local times=0
			while times < 10 do
				if _p.x != px or _p.y != py then
					times += 1
					px,py=_p.x,_p.y
				end
				yield()
			end
		end),
  "you can select special tricks",true,
  "by pressing ".._xbtn..".",true,
		"you use special moves by holding",true,
		"down ".._obtn.." and using the arrow keys.",true,
  "select dash and use it.",false,
		cocreate(function()
			local timer = 0
			while _p.counters["dash"] < 1 do
				timer += 1
				if timer % 300 == 0 then
					uiinfo("press ".._xbtn.." to select special move.")
				end
				if timer % 390 == 0 then
					uiinfo("hold ".._obtn.." and use arrows to use.")
				end
				yield()
			end
		end),
		"dashing is fast but makes noise.",true,
		"so does bumping into stuff.",true,
		"noise is visible in the dark.",true,
		"moves can succeed or fail...",true,
		"this is indicated by green",true,
		"or red flashes.",true,
		"failure tells you a move",true,
		"did nothing.",true,
		"run into something.",false,
		cocreate(function()
			while true do
				local found=false
				-- todo: uieach?
				for e in all(uis) do
					if e.boxkind == "failure" then
						found=true
					end
				end
				if found then break end
				yield()
			end
		end),
		"use the indicators to help count",true,
		"your steps in the dark.",true,
		"dashing and sneaking are the",true,
		"only way you can move.",true,
		function()
			tutorial_dummy=edummy(-1,-1)
   tutorial_dummy.hp=2
			while not mplace(tutorial_dummy) do
			end
			add_move("sense area")
   sfx(24)
		end,
		"* acquired 'sense area'! *",true,
		"sense area uses indicators",true,
		"to inform you where things are",true,
		"in the dark.",true,

		"sense area succeeds when there",true,
		"is an enemy in a rectangle area",true,
		"sweep of the picked direction.",true,

		"there is a dummy in the room.",true,
		"select 'sense area'",false,
		cocreate(function()
			local _,mv=find_move("dash")
			while _p.move != mv do
				yield()
			end
		end),
		"practice sensing the dummy",false,
		cocreate(function()
			local timer=0
			while _p.counters["sense area"] < 3 do
				timer += 1
				yield()
			end
		end),
		function()
			local tutorial_torch=etorch(-1,-1)
   while not mplace(tutorial_torch) do end
   mfov()
			while not mplace(tutorial_dummy) or mlit(tutorial_dummy.x,tutorial_dummy.y) do end
		end,
		"...",true,
		"the dummy has teleported!",true,
		"practice sensing in the dark.",false,
		cocreate(function()
			local timer=0
			while _p.counters["sense area"] < 6 do
				timer += 1
				yield()
			end
		end),
		function()
			add_move("slash")
			add_move("light match")
   disable_moves(false)
   _p.mp = _default_mp
   sfx(24)
		end,
		"* acquired 'slash'! *",true,
		"slash attacks the three tiles",true,
		"adjacent to you in a direction.",true,
		"it will indicate success when it",true,
		"deals damage to something.",true,
		"find the dummy and attack it.",false,
		cocreate(function()
			while tutorial_dummy.hp != 1 do
				yield()
			end
		end),
		function()
			tutorial_enemy=eguard(-1,-1)
			while not mplace(tutorial_enemy,"wall") or not mlit(tutorial_enemy.x,tutorial_enemy.y) do
			end
			add_move("throw knife")
			_p.mp=_default_mp
			_p.hp=2
   disable_moves(false)
			game_on = true
   sfx(24)
		end,
		"* acquired 'throw_knife'! *",true,
  function()
   sfx(57)
  end,
		"guard: nooo! not dum-dum!",true,
		"encounters are broken up into",true,
		"turns.",true,
		"every turn you get 3 energy to",true,
		"perform any special you want.",true,
		"you can only use one specific",true,
		"special per turn. utilize others.",true,
		"use throw knife from a distance.",false,
		cocreate(function()
			while true do
				if _p.hp == 1 then
					tutorial_enemy.lifetime=0
					uiinfo("...?")
					break
				elseif tutorial_enemy.hp == 0 then
				 uiinfo("got him!")
				 break
				end
				yield()
			end
			reveal_doors()
			make_things_interract()
			tutorial_moves=_moves
		end),
		"play around with other moves.",true,
		"the doors lead to the mansion.",true,
		"take the amulet back.",true,
		nil
	}
	next_script()
end

function tutorial_update()
	base_update()
	blink_timer -= 1
	if blink_timer == 0 then
		blink_timer=6
		blink=not blink
	end

	if current_script == true then
		if btnp(5) then
			if info_msg.scroll < #info_msg.msg then
				info_msg.scroll=#info_msg.msg
			else
				next_script()
			end
		end
	elseif _p then

		if move_control(_p,tutorial_moves,function()
			_update=tutorial_update
			_draw=tutorial_draw
		end,true) then
			return
		end

		local bump=base_single_update(function(b)
   game_init()
  end, not game_on)
	end

	if type(current_script) == "thread" then
		assert(coresume(current_script))
		if costatus(current_script) == "dead" then
			next_script()
		end
	end

	if not game_on then
		disable_moves(false)
	end
end

function tutorial_draw()
	if  not base_draw() then return end
	if current_script == true and blink then
		print("❎",120,120,5)
	end
	if _p then
  energy(_p.mp,8,-1)
	end
end

-- ***

local rogue
local ret_cb
local notify
local move_list

function move_select_init(r,cb,note,mv)
	_draw=move_select_draw
	_update=move_select_update
	rogue=r
	ret_cb=cb
	notify=note
	move_list=mv or _moves
end

function move_select_draw()
	cls()
	local sel=arrow_menu(
		"select a trick...",
		"press ❎ to cancel",
		rogue.player,move_list,not notify)

	if sel == -1 then
		ret_cb()
		return
	elseif type(sel) == "number" then
		if move_list[sel] then
			rogue.move=move_list[sel]
			if notify then
				uiinfo("selected '" .. move_list[sel].name .. "'!")
			end
		end
		ret_cb()
		return
	end

	uidraw()
end

function move_select_update()
	uiupdate()
end

local tit,timer,msg

function game_over_init(t,m,single_player)
 tit=t
	timer=0
	msg=m
	_update=game_over_update
	if single_player then
		_draw=game_over_draw
	else
	 _draw=versus_over_draw
	end
 --music(1)
end

function game_over_update()
	timer += 1
	uiupdate()
	if (btnp(5)) menu_init()
end

function versus_over_draw()
	cls()
	if timer > 0 then
  font(tit,64-(#tit/2)*8-8,0)
		printc(msg,64,16,5)
  printc("press ❎ to return to main menu",64,120,5)
	end
 color(7)
	printc("p1",32,3*8-8,7)
	printc("p2",96,3*8-8,7)

	local limit=10
	local y=3*8

	for mv in all(_moves) do
		local name=mv and mv.name
		if name then
			if timer < limit then
				print(rnddig() .. rnddig() .. " " .. name,
					0,y)
				print(rnddig() .. rnddig() .. " " .. name,
					64,y)
				break
			end
			print(tostr(_p.counters[name]) .. " " .. name,
				0,y)
			print(tostr(_p2.counters[name]) .. " " .. name,
				64,y)
			y += 8
			limit += 20
		end
	end

	uidraw()
end

function game_over_draw()
	cls()
	if timer > 0 then
  font(tit,64-(#tit/2)*8-8,0)
		printc(msg,64,16,7)
  printc("press ❎ to return to main menu",64,120,5)
	end
 color(7)
	local limit=10
	local y=5*8
	local i=1
	local x=0

	for mv in all(_moves) do
		if i == 9 then
			y=5*8
			x=64
		end
		local name=mv and mv.name
		if name then
			if timer < limit then
				print(rnddig() .. rnddig() .. " " .. name,
					x,y)
				break
			end
			print(tostr(_p.counters[name]) .. " " .. name,
				 x,y)
			y += 8
			limit += 10
		end
		i += 1
	end

	uidraw()
end

-->8
-- game stuff

local eyes={"opened","closed"}
local sides={0,1,2,3}

function is_wall_of_side(wall,side)
	local start=20+(side*2)

	for i=start,start+16,16 do
		if wall==i or wall==i+1 then return true end
	end
	return false
end

local mansion

function init_mansion()
	local tmp_sides={0,1,2,3}
	local o_sides=0
	local c_sides=0
	while #tmp_sides > 0 do
		local d=tmp_sides[dice(1,#tmp_sides)]
		del(tmp_sides,d)
		local which
		if rnd(1) < 0.5 then
			o_sides=bor(o_sides,shl(1,d))
		else
		 c_sides=bor(c_sides,shl(1,d))
		end
	end
 uiinfo(tostr(o_sides) .. " " .. tostr(c_sides))
	mansion={
		prev_eyes=nil,

		rules={
			opened=o_sides,
			closed=c_sides
		}
	}
end

function go_thru(door)
	local p=mansion.prev_eyes
	mansion.prev_eyes=door.eyes
	return not p or band(mansion.rules[p],shl(1,door.side)) > 0
end

function random_wall(side)
	local walls={}
	miter(function(x,y)
		if is_wall_of_side(mget(x,y),side) then
			add(walls,{x,y})
		end
	end)
	local w=choose(walls)
	return w[1],w[2]
end

function reveal_doors()
	mlight_all(true)

	for s=0,3 do
		local x,y=random_wall(s)
		local door=edoor(x,y,s,rnd(1) < 0.5 and "opened" or "closed")
		mset(x,y,34)
	end
end

function make_things_interract()
	eeach(function(e)
		if not e.hp then
			e.interact=true
		end
	end)
end

-->8
-- board stuff
--room generation,shadow cast

function mlit(x,y)
	return fget(mget(x,y),2)
end

function msolid(x,y)
	local t=mget(x,y)
	return t == 0 or fget(t,0)
end

function mfeel(x,y)
	if msolid(x,y) then
		return mget(x,y)
	end
	return eget(x,y)
end

function miter(cb)
	for x=0,_mw-1 do
		for y=0,_mh-1 do
			local ret=cb(x,y)
			if ret then return ret end
		end
	end
end

function mclear()
	for x=0,_mw-1 do
		for y=0,_mh-1 do
			mset(x,y,0)
		end
	end
end

function mplace(obj,where)
	local x,y
	local c=0.5
	if where == "wall" then
		c=1.0
	else
	 c=0.0
	end
	if rnd(1) < c then
		x,y=rnd_wall()
	else
	 x,y=rnd_room()
	end
	if not mfeel(x,y) then
		obj.x,obj.y=x,y
		return obj
	end
end

function mgenerate()
	mclear()
	local n=1
	for x=0,_mw-1 do
		for y=0,_mh-1 do
			local s=mget(
				n*_mw+x,
				0*_mh+y)
			mset(x,y,s)
		end
	end

	local col_count=dice(3,4)-5
	local torch_chance=0.6

	while col_count > 0 do
		col_count -= 1
		local x,y=rnd_room()
		local col_found=false
		for d in all(_deltas) do
			local t=mget(x+d[1],y+d[2])
			if t == 8 then
				col_found=true
				break
			end
		end
		if not col_found then
			local d=_deltas[ceil(rnd(4))*2]
			mset(x,y,8)
			local nx,ny=x+d[1],y+d[2]
			if rnd(1) < torch_chance and not eget(nx,ny) then
				etorch(nx,ny)
			end
		end
	end

	local furniture_count=dice(3,4)-5
	local furnitures={etable,echair,eshelf}

	while furniture_count > 0 do
		furniture_count -= 1
		local furn=choose(furnitures)
		mplace(furn(-1,-1))
	end
end

function mpopulate(diff)
	local rates=choose(_enemy_rates[diff])

	for enemy,number in pairs(rates) do
		for i=1,number do
			-- infinite loop?
			while true do
				local x,y=rnd_room()
				if not mfeel(x,y) then
					if (enemy == "guards") eguard(x,y)
					if (enemy == "dogs") edog(x,y)
					if (enemy == "dummies") edummy(x,y)
					break
				end
			end
		end
	end
end

function mlight(x,y,on)
	if mlit(x,y) and not on then
		mset(x,y,mget(x,y)-1)
	elseif not mlit(x,y) and on then
		mset(x,y,mget(x,y)+1)
	end
end

function mlight_all(on)
	for x=0,_mw-1 do
		for y=0,_mh-1 do
			mlight(x,y,on)
		end
	end
end

function mfov()
	mlight_all(false)

	eeach(function(torch)
		if not torch.lit then return end
		fov({x=torch.x,y=torch.y},torch.lit,function(x,y)
			if not mlit(x,y) then
				mset(x,y,mget(x,y)+1)
			end
		end)
	end)
end

function castl(
	cx,cy,row,start,fin,
	radius,xx,xy,yx,yy,
	cb)
	if (start < fin) return
	local rad_sq=radius*radius
	for j=row,radius do
		local dx,dy=-j-1,-j
		local blocked=false
		while dx <= 0 do
			dx += 1
			local tx,ty =
				cx+dx*xx+dy*xy,
				cy+dx*yx+dy*yy
			local t=mget(tx,ty)
			local l_sope,r_slope =
				(dx-0.5)/(dy+0.5),
				(dx+0.5)/(dy-0.5)
			if start < r_slope then

			elseif fin > l_sope then
				break
			else
				if dx*dx+dy*dy < rad_sq then
					cb(tx,ty)
				end
				if blocked then
				 if fget(t,1) then
						new_start=r_slope
					else
						blocked=false
						start=new_start
					end
				else
					if fget(t,1) then
						blocked=true
						castl(cx,cy,j+1,
							start,l_sope,
							radius,xx,xy,yx,yy,
							cb)
						new_start=r_slope
					end
				end
			end
		end
		if blocked then break end
	end
end

function fov(e,rad,cb)
	local mult={
		{1,0,0,-1,-1,0,0,1},
		{0,1,-1,0,0,-1,1,0},
		{0,1,1,0,0,-1,-1,0},
		{1,0,0,1,-1,0,0,-1},
	}
	cb(e.x,e.y)
	for oct=1,8 do
		castl(
			e.x,e.y,1,1.0,0.0,rad,
			mult[1][oct],mult[2][oct],
			mult[3][oct],mult[4][oct],
			cb)
	end
end

-->8
-- entities stuff
-- minimal entities

local ents={}

function eget(x,y)
	for e in all(ents) do
		if e.x == x and e.y == y then
			return e
		end
	end
end

function eadd(e)
	add(ents,e)
	e.x=flr(e.x)
	e.y=flr(e.y)
	return e
end

function edraw()
	for e in all(ents) do
		if e.anim and (mlit(e.x,e.y) or e.nohide) then
			e:anim()
		end
	end
end

function eupdate()
	for e in all(ents) do
		if e.lifetime then
			if e.lifetime == 0 then
				del(ents,e)
			else
				e.lifetime -= 1
			end
		end
	end
end

function eturn()
	for e in all(ents) do
		if e.ai then
			e:ai()
		end
	end
end

function eclear()
	ents={}
end

function enemies(to)
 for e in all(ents) do
  if e.hp and e != (to or _p) then
   return true
  end
 end
end

function eeach(cb)
	foreach(ents,cb)
end

function attack(e)
	if e.hp then
  boom(e.x,e.y,"loud","blood")
		e.hp -= 1
  sfx(54)
		if e.hp == 0 then
			del(ents,e)
		end
		return true
	elseif e.lit then
  e.lit -= 1
  sfx(51)
  if e.lit == 0 then
 		del(ents,e)
  end
  mfov()
		return true
	end
	return false
end

function erogue(x,y,plr)
	local p=64
	if plr == 1 then p=80 end

	local function counters()
		local c={}
		for m in all(_moves) do
			if m then
				c[m.name]=0
			end
		end
		return c
	end

	return eadd {
		x=x,
		y=y,
		hp=1,
		mp=_default_mp,
		move=nil,
		counters=counters(),
		anim=animate(p,p+3,6),
		player=plr,
		last_sounds={},
		do_rogue=function(self)
			local bump
			local mv
			if not btn(4,plr) then
				mv={f=sneak}
			elseif btn(4,plr) and self.move and not self.move.disabled then
				mv=self.move
			end

			if mv then
				if btnp(0,plr) then
					bump=mv.f(self,-1,0)
				elseif btnp(1,plr) then
					bump=mv.f(self,1,0)
				elseif btnp(2,plr) then
					bump=mv.f(self,0,-1)
				elseif btnp(3,plr) then
					bump=mv.f(self,0,1)
				end
			end

			if (btnp(0,plr) or
							btnp(1,plr) or
							btnp(2,plr) or
							btnp(3,plr)) and mv then
				self.mp -= 1
				if mv.name then
					self.counters[mv.name] += 1
					mv.disabled = true
				end
			end

			if eis(bump,"interact") and eis(bump,"furn") then
				uiinfo(bump.text)
			end

			return bump
		end
	}
end

function eis(e,kind)
	return type(e) == "table" and e[kind]
end

function eguard(x,y)
	local sprites={
		96,112,72
	}
	local pick=sprites[dice(3)]
	return eadd {
		x=x,
		y=y,
		hp=1,
		ai=guard_ai,
  stumble=0.02,
		state="sleeping",
		adhd=dice(6,3),
		adhd_t=0,
		turn_mp=function() return dice(2) end,
		path={},
		anim=animate(pick,pick+3,6)
	}
end

function edog(x,y)
	return eadd {
		x=x,
		y=y,
		hp=1,
		ai=dog_ai,
  stumble=0.01,
		bark=dice(8,3),
  smell=rnd(0.2),
		state="sleeping",
		adhd=dice(6,3),
		adhd_t=0,
		turn_mp=function() return dice(3) end,
		path={},
		anim=animate(85,86,8)
	}
end

function edummy(x,y)
	return eadd {
		x=x,
		y=y,
		hp=1,
		ai=nil,
		anim=animate(101,102,15)
	}
end

function etorch(x,y)
	return eadd {
		x=x,
		y=y,
		anim=animate(11,14,4),
		lit=4
	}
end

function edoor(x,y,side,eyes)
	return eadd {
		x=x,
		y=y,
		side=side,
		eyes=eyes,
		door=true,
		nohide=true,
		anim=eyes == "opened" and animate(2,3,15) or animate(4,5,0)
	}
end

function ebomb(x,y)
	return eadd {
		x=x,
		y=y,
		lifetime=30,
		anim=function(self)
			if self.lifetime == 1 then
				self.lit=nil
				mfov()
			end
			animate(11,14,4)
		end,
		lit=2
	}
end

function echair(x,y)
	return eadd {
		x=x,
		y=y,
		anim=animate(46,46,0),
		furn=true,
		text="i don't know how to use this."
	}
end

function etable(x,y)
	return eadd {
		x=x,
		y=y,
		anim=animate(30,30,0),
		furn=true,
		text="hmm. no food."
	}
end

function eshelf(x,y)
	return eadd {
		x=x,
		y=y,
		anim=animate(62,62,0),
		furn=true,
		text=choose(_texts)
	}
end

-->8
-- ai

function tremove(tab,idx)
	local tmp=tab[idx]
	tab[idx]=tab[#tab]
	tab[#tab]=nil
	return tmp
end

function new_node(x,y,parent)
	return {
		x=x,
		y=y,
		g=0,h=0,f=0,
		parent=parent
	}
end

function pop_lowest_f(list)
	local found=1
	for i=1,#list do
		if list[i].f < list[found].f then
			found=i
		end
	end
	return tremove(list,found)
end

function manhatann(s,e)
	return abs(s.x-e.x)+abs(s.y-e.y)
end

function xykey(n)
	return tostr(n.x) .. "," .. tostr(n.y)
end

function astar(sx,sy,gx,gy)
	local start=new_node(sx,sy)
	local goal=new_node(gx,gy)

	local opened={start}
	local opened_idx={[xykey(start)]=true}
	local closed={}

	local node

	while #opened > 0 do
		node=pop_lowest_f(opened)
		opened_idx[xykey(node)]=false

		closed[xykey(node)]=true

		if node.x == gx and node.y == gy then
			break
		end

		local new
		local ds={{0,-1},{0,1},{-1,0},{1,0}}
		while #ds > 0 do
			local d=choose(ds)
			del(ds,d)
			local x,y=node.x+d[1],node.y+d[2]

			local e=eget(x,y)

			if not msolid(x,y) and
					(e == _p or not e or not mlit(x,y)) then
				new=new_node(x,y,node)

				if not closed[xykey(new)] then
					new.g=node.g+1
					new.h=manhatann(new,goal)
					new.f=new.g+new.h

					if not opened_idx[xykey(new)] then
						add(opened,new)
						opened_idx[xykey(new)]=true
					end
				end
			end
		end
	end

	local path={}
	while node do
		add(path,node)
		node=node.parent
	end

	path[#path]=nil

	return path
end

function ai_move(e,tx,ty)
	local what=mfeel(tx,ty)
	if not what then
		e.x=tx
		e.y=ty
		return "moved"
	else
		if eis(what,"trap") then
			what.lifetime=0
			boom(e.x,e.y,"loud")
			e.x=tx
			e.y=ty
   sfx(52)
		elseif what == _p then
			attack(_p)
			return "hit"
		else
		 boom(e.x,e.y,"normal")
   sfx(48)
		end
	end
end

function ai_travel(self)
	local to=self.path[#self.path]
	local happened=ai_move(self,to.x,to.y)
	if happened == "moved" then
	 self.path[#self.path]=nil
	 return true
	elseif happened == "hit" then
		return true
	end
end

function ai_aquire_target(self)
	if #_p.last_sounds > 0 then
			local somewhere=choose(_p.last_sounds)
			self.path=astar(self.x,self.y,somewhere.x,somewhere.y)
			return #self.path > 0
		end
end

ais={}

function ais.guard_sleeping(self)
	if #_p.last_sounds > 0 then
		self.state="seeking"
	else
	 return true
	end
end

function ais.guard_charging(self)
	if not mlit(_p.x,_p.y) then
		self.state="seeking"
		self.path=astar(self.x,self.y,
			_p.last_seen.x,_p.last_seen.y)
	elseif #self.path == 0 then
		-- weird case?
		self.state="seeking"
	else
		-- even if we bump,we will
		-- reaquire because charging
		ai_travel(self)
		return true
	end
end

function ais.guard_seeking(self)
	if #self.path == 0 then
		-- todo: spiral
		ai_aquire_target(self)

		if #self.path == 0 then
			-- can happen if the only sound
			-- is on this guy,just wander
			self.state="wandering"
			return
		end
	end
	if not ai_travel(self) then
		if not ai_aquire_target(self) then
			self.state="wandering"
		end
	end
	return true
end

function ais.guard_wandering(self)
	if not ai_aquire_target(self) then
		local d=_deltas[2*dice(1,4)]
  local x,y = self.x+d[1],self.y+d[2]
  if not mfeel(x,y) or rnd(1) < self.stumble then
		 ai_move(self,x,y)
		 return true
  else
   return false
  end
	end
	self.state="seeking"
end

function guard_ai(self)
	ai(self,"guard_")
end

ais.dog_sleeping=ais.guard_sleeping
ais.dog_charging=ais.guard_charging
ais.dog_seeking=ais.guard_seeking
ais.dog_wandering=function(self)
 if rnd(1) < self.smell then
  self.path=astar(self.x,self.y,_p.x,_p.y)
  self.state="seeking"
 else
  return ais.guard_wandering(self)
 end
end

function dog_ai(self)
	self.bark -= 1
	if self.bark == 0 then
		self.bark=dice(8,3)
		boom(self.x,self.y,"loud")
		uiinfo("dog: bark!")
  if self.state == "sleeping" then
   self.state="wandering"
  end
		sfx(58)
	end
	ai(self,"dog_")
end

function ai(self,prefix)
	self.adhd_t += 1

	-- ai directives
	if mlit(_p.x,_p.y) then
		self.state="charging"
		self.path=astar(self.x,self.y,_p.x,_p.y)
	elseif self.adhd_t >= self.adhd and self.state != "charging" then
		self.adhd_t=0
  if self.guard then
   sfx(57)
  end
		-- switch target
		if ai_aquire_target(self) then
			self.state="seeking"
		else
		 self.state="wandering"
		end
	end
	local moves=self.turn_mp()
	while moves > 0 do
		if ais[prefix .. self.state](self) then
			moves -= 1
		end
	end
end

__gfx__
0000000000000000e777777ee777777ee777777ee777777ee777777ee777777eee5555eeee7777eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000
000000000000000070000007700000077000000770000007705057e77007eee7e500005ee700007eeeeeeeeeeee7eeeeeee7eeeeeeee7eeeeee7eeee00000000
007007000000000070550557705505577000000770000007700007e77000eee75005000570070007eeeeeeeeeee7eeeeee7e7eeeee77eeeeee777eee00000000
00077000000000007050050770050057705505577055055770000ee77007eee75000000570000007eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000
000770000000000070000007700000077000000770000007700007e77007eee75000050570000707ee777eeeee777eeeee777eeeee777eeeee777eee00000000
007007000000000070000707700007077000070770000707700007e77007eee75050000570700007ee717eeeee717eeeee717eeeee717eeeee717eee00000000
00000000000000007000000770000007700000077000000770077ee7707eeee7e500005ee700007eeee7eeeeeee7eeeeeee7eeeeeee7eeeeeee7eeee00000000
000000000000000070000007700000077000000770000007777eeee777eeeee7ee5555eeee7777eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000
000000000000000000000000000000005eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee75555555577777777eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
000000000000000000000000000000005eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
000000000000000000000000000000005eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeee77777777777777ee777777eeeeeeeee
000000000000000000000000000000005eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeee70000000000007ee700007eeeeeeeee
000000000000000000000000000000005eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeee70000000000007ee700007eeeeeeeee
055005500770077005500550077007705eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeee77777777777777ee777777eeeee77ee
000550000077770000055000007777005eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeeee700000000007eeee7007eeeeeeeeee
000000000000000000000000000000005eeeeeee7eeeeeee5555555577777777eeeeeee5eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee77777777
000000000000000000000000000000007eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeeee50eeeeee700505050507070707eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
0000000000000000000000000000000007eeeeee05eeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee75e5e5e5e7e7e7e7eeeeeeeeeeeeeeeeeeeeee7eeeeeeeeee
000000000000000000000000000000007eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeeee50eeeeee70eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee7eeeeeeeeee
0000000000000000000000000000000007eeeeee05eeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee7777eeeeeeeeee
000000000000000000000000000000007eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeeee50eeeeee70eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee7117eeeeeeeeee
0005500000077000000000000000000007eeeeee05eeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee7777eeeeee55ee
000550000077770000055000007777007eeeeeee5eeeeeeee5e5e5e5e7e7e7e7eeeeee50eeeeee70eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee7007eeeeeeeeee
0000000000000000000000000000000007eeeeee05eeeeee5050505e70707070eeeeeee5eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee55555555
550000556600006600000000000000007eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee75555555577777777eeeeeeeeeeeeeeeee777777eeeeeeeee
500000056000000600000000000000007eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee700707eeeeeeeee
000000000000000000000000000000007eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeee5e5eeeee7e7ee55eeeeee77eeeee777777ee777777ee777777eeeeeeeee
000000000000000000000000000000007eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeee5e5eeeee7e7eeeeeeeeeeeeeeeee700007ee700007ee707007ee700007e
000000000000000000000000000000007e7eeeee5e5eeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeee700007ee777777ee777777ee070070e
000000000000000000000000000000007e7eeeee5e5eeeeeeeee55eeeeee77eeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeee777777ee700007ee700707ee707707e
500000056000000600555500077777707eeeeeee5eeeeeeeeeeeeeeeeeeeeeeeeeeeeee5eeeeeee7eeeeeeeeeeeeeeeee700007ee777777ee777777ee070070e
550000556600006600000000000000007eeeeeee5eeeeeee5555555577777777eeeeeee5eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeee77eeeeee55eeeeeeeeeeeeee55ee00000000eeeeeeeeeeeeeeee00000000eeee77eeeee77eeeee77eeeeeee77eee00000000000000000000000000000000
e77077e7e55055eee77077eee55055ee00000000eeeeeeeeeeeeeeee00000000e7707707e707707e7077077ee707707e00000000000000000000000000000000
7000ee7e5000eeee700077ee5000eeee00000000eeeeeeeeeeeeeeee000000007000000770000007700000077000000700000000000000000000000000000000
700777ee5005eee57007eee75005eee500000000e77eeeeee77eeeee000000007000000770000007700000077000000700000000000000000000000000000000
7007eeee5005555e7007777e5005555e00000000777eeeeee777ee7e000000007000000770000007700000077000000700000000000000000000000000000000
7777eeee5555eeee7777eeee5555eeee00000000ee77777eee7777ee000000007777777777777777777777777777777700000000000000000000000000000000
7eee7eee5eee5eee7ee7eeee5eee5eee00000000ee7777eeee7777ee000000007eeeeee77eeeeee77eeeeee77eeeeee700000000000000000000000000000000
7eee7eee5eee5eee7ee7eeee5eee5eee00000000ee7ee7eeee7ee7ee000000007eeeeee77eeeeee77eeeeee77eeeeee700000000000000000000000000000000
eee77eeeeee55eeeeeeeeeeeeee55eee00000000eeeeeeeeeeeeeeee000000000000000000000000000000000000000000000000000000000000000000000000
7ee7707eeee5505eeee77eeeeee5505e00000000eeeeeeeeeeeeeeee000000000000000000000000000000000000000000000000000000000000000000000000
e7e000075ee00005eee7707e5ee0000500000000eeeee77eeeeeeeee000000000000000000000000000000000000000000000000000000000000000000000000
ee770007e5e500057ee00007e5e50005000000007e777777eeeee77e000000000000000000000000000000000000000000000000000000000000000000000000
eee70007ee550005e7e70007ee55000500000000e70007ee7e777777000000000000000000000000000000000000000000000000000000000000000000000000
eee77777eee55555ee770007eee5555500000000e77777eee70007ee000000000000000000000000000000000000000000000000000000000000000000000000
eee7eee7eee5eee5eee77777eee5eee500000000e7eee7eee77777ee000000000000000000000000000000000000000000000000000000000000000000000000
eee7eee7eee5eee5eee7eee7eee5eee500000000e7eee7eee7eee7ee000000000000000000000000000000000000000000000000000000000000000000000000
ee7ee77ee7ee77ee7ee77eeee7ee77ee00000000eeeeeeeeeeeeeeee000000000000000000000000000000000000000000000000000000000000000000000000
ee7ee77ee7ee77ee7ee77eeee7ee77ee00000000eeeeeeeeeee77eee000000000000000000000000000000000000000000000000000000000000000000000000
ee7eeeeee7eeeeeee7eeeeeee7eeeeee00000000eee77eeeeee77eee000000000000000000000000000000000000000000000000000000000000000000000000
ee77777ee7e7777ee7e7777ee7e7777e00000000eee77eeeee7007ee000000000000000000000000000000000000000000000000000000000000000000000000
ee70077eee70707eee70707eee70707e00000000eee00eeeee7007ee000000000000000000000000000000000000000000000000000000000000000000000000
ee70007eee70007eee70007eee70007e00000000ee7007eeee7007ee000000000000000000000000000000000000000000000000000000000000000000000000
ee77777eee77777eee77777eee77777e00000000ee7007eeee7007ee000000000000000000000000000000000000000000000000000000000000000000000000
ee7eee7eee7eee7eee7eee7eee7eee7e00000000ee7777eeee7777ee000000000000000000000000000000000000000000000000000000000000000000000000
eee77eeeeee77eeeeeeeeeeeeee77eee00000000eeeeeeee00000000000000000000000000000000000000000000000000000000000000000000000000000000
eee7707eeee7707eeee77eeeeee7707e00000000eee77eee00000000000000000000000000000000000000000000000000000000000000000000000000000000
7777007eeee7007eeee7707eeee7007e00000000eee77eee00000000000000000000000000000000000000000000000000000000000000000000000000000000
7007007e7777007e7777007e7777007e00000000ee7007ee00000000000000000000000000000000000000000000000000000000000000000000000000000000
7007007e7007007e7007007e7007007e00000000ee7007ee00000000000000000000000000000000000000000000000000000000000000000000000000000000
e777777e7007777e7007007e7007777e00000000ee7007ee00000000000000000000000000000000000000000000000000000000000000000000000000000000
ee7eee7ee77eee7ee777777ee77eee7e00000000ee7007ee00000000000000000000000000000000000000000000000000000000000000000000000000000000
ee7eee7eee7eee7eee7eee7eee7eee7e00000000ee7777ee00000000000000000000000000000000000000000000000000000000000000000000000000000000
eeeee7eeeeeeeeeeeeeee7eeeeeeeeeeeeeeeeee00000000eeeeeeeeeeee7eeeeeeeeeee0000000000000000000000000000000000000000eee77eee00000000
eeeeeeeeeeeeeeeeeeeeeeeeeeeee7eeeeeeeeee00000000ee777eeeeee77eeeee77e77e0000000000777700007777000077770000000000ee77eeee00000000
eeeeeeeeeeee77eeeeeeeeeeeeeeeeeee7eeeeee00000000e70007eeeee77eeee70070070000000007777770077777700777777000000000e777777e00000000
eeeeeeeeeeee77eeeeeeeeeeeeeeeeeeeeeeeeee000000007000707eeee77eeee70000070000000001777770077117700777711000000000e777777e00000000
e77eeeeee7eeeeeeeeeeeeeeee77eeeeeeeeeeee000000007007007eee7777eee70000070000000007777770077777700777777000000000ee77eeee00000000
e77eeeeeeeeeeeeee77eeeeeee77eeeeeeeee7ee000000007070007eeee00eeeee70007e0000000007777710017777700711777000000000eee77eee00000000
eeeeeee7eeeeee7ee77eeeeeeeeeeeeeeeeeeeee00000000e70007eeeee77eeeeee707ee0000000000777700007777000077770000000000eeeeeeee00000000
eeeeeeeeeeeeeeeeeeeeee7eeeeeeeeeeeeeeeee00000000ee777eeeeeee7eeeeeee7eee0000000000000000000000000000000000000000eeeeeeee00000000
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee6eeeeee00000000eeeeeeeeeeeeeeeeeeeeeeee0000000000000000000000000000000000000000eee77eee00000000
eeeeeeeeeeeeeeeeeee7eeeeee6eeeeeeeeeeee600000000e7eee7eee7eee7eeeeeeee7e0000000000000000000000000000000000000000ee7777ee00000000
eeeeeeeeeeee77eeeeeee77eeeeeee6eeeeeeeee00000000ee1e1eee777e777eeeeee7770000000000077000000770000001700000000000e777777e00000000
eeeeeeeeeeee77eeeeeee77eeeeeeeeeeeeeeeee00000000eee1eeeee77777eeeeee777e0000000000777700007777000077770000000000e7e77e7e00000000
eee777eeeee77eeee77eeeeeeeeeeeeeeeeeeeee00000000ee1e1eeeee777eeee7e777ee0000000000177700007717000077770000000000eee77eee00000000
ee7777eeee777eeee77eeeeeeeeeeeeeeeeeeeee00000000e7eee7eee77777ee77777eee0000000000077000000770000007700000000000eee77eee00000000
ee7777eeee77eeeeeeeeeeeeeeeeeeeeeeeeeeee00000000eeeeeeee777e777ee777eeee0000000000000000000000000000000000000000eeeeeeee00000000
eee77eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000eeeeeeeee7eee7eeee7eeeee0000000000000000000000000000000000000000eeeeeeee00000000
eeeeeeeee7e7eeeeeeeeeeeeeeeee7ee0000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000000000000eee77eee00000000
eeeeeeeeeee77ee77777e7eeeeeeeeee0000000000000000eeeee7eeeeeee7eeeeee7eeeeeee7eee00000000000000000000000000000000eeee77ee00000000
eeeeeeeeee777eee77777eee7ee11eee0000000000000000e77e77eee77e77eeeeee7eeeeeee7eee00000000000000000000000000000000e777777e00000000
eee7eeeee7777777e11117eeee1111ee0000000000000000ee777eeeee111eeee7777eeee1171eee00071000000770000007700000000000e777777e00000000
eeeeeeeeee777777e711177eeee111e70000000000000000eee777eeeee117eeeee7777eeee7177e00077000000770000001700000000000eeee77ee00000000
eeeeeeeee77777eee771777eeeee1eee0000000000000000ee77e77eee77e17eeee7eeeeeee1eeee00000000000000000000000000000000eee77eee00000000
eeeeeeeee7ee77eeeee177eeee7e1eee0000000000000000ee7eeeeeee1eeeeeeee7eeeeeee7eeee00000000000000000000000000000000eeeeeeee00000000
eeeeeeeeeeeee7eeeee77eeeeeeeeeee0000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000000000000eeeeeeee00000000
eee7eeeeeeeee7eeeeeeeeeeeeeeeeee7eeeeee7eeeeeeeeeeeeeeeeeeeeeeeeee7e7eee00000000eeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeeee77eee00000000
eeeeeeeee7eeeeeeeeeeeeeeeeeeeeeeeeee7eeeeeeeeeeeeeeeeeeeee7e7eeeeeeeee7e00000000eeeeee7eeeeeee7eeeeeeeeeeeeeeeeeeee77eee00000000
eeeeeeeeeeeeeeeeeeeeee7eeeeeeeeee7eeeeeeeeeeeeeeeee7eeeee7eeee7e7eeeeeee00000000eeeeeeeeeeeee7eeeeeee7eeeeeeeeeee7e77e7e00000000
eeeeeeeeeeee7eeeee7eeeeeeeee7eeeeeeeee7eeee7eeeeee7ee7eee7eeeeeeeeeeeee700000000eeeeeeeeeeee7eeeeeee7eeeeeeeeeeee777777e00000000
eeeee7eeee7eeeeeeeeeeeeeeeeeee7eeeeeeee7eeeeeeeeee7ee7eeeeeeee7eeeeeeeee00000000eeeeeeeeeee7eeeeeee77eeeeeee7eeeee7777ee00000000
e7eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee7eeeee7eeee7e7eeeeee700000000eeeeeeeeee7eeeeeee77eeeeeee7eeeeeee77eee00000000
eeeeeeeeeeeeee7eeeeeee7ee7eeeeee7eeeeeeeeeeeeeeeeeeeeeeeee77eeeee7eeeeee00000000eeeeeeeeeeeeeeeee77eeeeee77eeeeeeeeeeeee00000000
eeeeeeeee7eeeeeeeeeeeeeeeeeeeeeeee7eeee7eeeeeeeeeeeeeeeeeeeeeeeeeeee7eee00000000eeeeeeeeeeeeeeee7eeeeeee7eeeeeeeeeeeeeee00000000
00000000070000700000000000000000000070000007070000000000000000000007000000000000000000000000000000000000000000000000000000000000
00000000070000700000000000000000000070000007070000000000000000000007000000070000000070000000000000000000000000000000000000000000
00700700070000700000000000000000000070000000000000070700077000700000000000700000000007000070707000007000000000000000000000000000
00077000070000700000000000000000000070000000000000777770077007000000000000700000000007000007770000007000000000000000000000000000
00077000070000700000000000000000000070000000000000070700000070000000000000700000000007000007770000777770000000000077770000000000
00700700070000700000000000000000000070000000000000777770000700000000000000700000000007000070707000007000000000000000000000000000
00000000070000700000000000000000000000000000000000070700007007700000000000700000000007000000000000007000000070000000000000000000
00000000070000700000000000000000000070000000000000000000070007700000000000070000000070000000000000000000000700000000000000007000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000700007777000000700007777770077777000700007007777770007777000077777000777000007777700000000000000000000070000000000000070000
00007000070070700007700000000070000070000700007007000000070000000000007000700700007000700000700000007000000700000077770000007000
00070000070700700000700000777700000777000777777007777770077777700000070000777700000777700000000000000000007000000000000000000700
00700000077000700000700007000000000007000000007000000070070000700000700007000070000000700000700000007000000700000077770000007000
07000000007777000007770007777770077770000000007000777700007777000000700007777770000777000000000000070000000070000000000000070000
00000000000000000700000000000000000000700000000000007700000000000700000000000000000000000700000000700000000000000000000000000000
00000000000000000700000000000000000000700000000000070000000000000700000000000000000000000700000000700000000000000000000000000000
00000000000000000700000000000000000000700000000000070000000000000700000000000700000007000700000000700000000000000000000000000000
00000000000777000777770000777770007777700077770000070000007777000700000000000000000000000700000000700000007777000777770000777700
00000000000000700700007007000000070000700700007000070000070000700777770000007700000007000700007000700000070070700700007007000070
00000000007777700700007007000000070000700777777000777700007777700700007000000700000007000777770000700000070070700700007007000070
00000000070000700700007007000000070000700700000000070000000000700700007000000700000007000700007000700000070000700700007007000070
00000000077777700777777000777770007777700777777000070000077777000700007000777770077770000700007000077700070000700700007000777700
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000077000000007700000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000070000000000700000000000000000000000000
00000000000000000000000000000000000700000000000000000000000000000000000000000000000000000070000000000700000000000000000000000000
00777700007777000077777000777770077777700700007007000070070000700700007007000070007777700070000000000700000000000000000000000000
07000070070000700700000007000000000700000700007007000070070000700700007007000070000000700070000000000700000000000000000000000000
07000070070000700700000000777700000700000700007000700070070070700077770000777770000777000070000000000700000000000000000000000000
07777700007777700700000000000070000700000700007000070700070070700700007000000070007000000070000000000700000000000000000000000000
07000000000000700700000007777700000077700077770000007000007707700700007007777700077777700077000000007700000000000000000000000000
__label__
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000700000000000000000070000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000700000000000000000070000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000700000000000000000070000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000007777700077770007000070077777000700007000777700000000000000000000000000000000000000000000000000
00000000000000000000000000000000070000700700007007000070070000700700007007000070000000000000000000000000000000000000000000000000
00000000000000000000000000000000070000700700007007000070070000700700007007777770000000000000000000000000000000000000000000000000
00000000000000000000000000000000070000700700007007000070070000700700007007000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000007777700077770000777700077777700700007007777770000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000070000000700007000000000000000000000007000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000070000000700007000000000000000000000007000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000070000000700007000000700000000000000007000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000077777000700007000000000077777000077777000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000070000700700007000007700070000700700007000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000070000700700007000000700070000700700007000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000070000700700007000000700070000700700007000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000077777700700007000777770070000700077777000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000700007000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000070000000700000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000777000007770000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000007777700077777000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000777000007770000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000070000000700000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000077777000777770000007070777077700770707007700000000000000000000000000000000000000000
00000000000000000000000000000000000000000000777007707700777000007070700070707000707070000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000770007707700077000007070770077007770707077700000000000000000000000000000000000000000
00000000000000000000000000000000000000000000777007707700777000007770700070700070707000700000000000000000000000000000000000000000
00000000000000000000000000000000000000000000077777000777770000000700777070707700077077000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000077777000777770000007700777070700000077077707770777000000000000000000000000000000000
00000000000000000000000000000000000000000000777007707770777000007070700070700000700070707770700000000000000000000000000000000000
00000000000000000000000000000000000000000000770007707700077000007070770070700000700077707070770000000000000000000000000000000000
00000000000000000000000000000000000000000000777007707700077000007070700077700000707070707070700000000000000000000000000000000000
00000000000000000000000000000000000000000000077777000777770000007070777077700000777070707070777000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000077777000777770000007770707077700770777077707770700000000000000000000000000000000000
00000000000000000000000000000000000000000000777007707700077000000700707007007070707007007070700000000000000000000000000000000000
00000000000000000000000000000000000000000000770007707700077000000700707007007070770007007770700000000000000000000000000000000000
00000000000000000000000000000000000000000000777007707770777000000700707007007070707007007070700000000000000000000000000000000000
00000000000000000000000000000000000000000000077777000777770000000700077007007700707077707070777000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000005550555055500550055000005550555055500550505005500000555005500000055055500500505055505500055055500000000000000000
00000000000000005050505050005000500000005050505050505050505050000000505050000000500050005050505050005050500050000000000000000000
00000000000000005550550055005550555000005550550055005050505055500000555055500000555055005050505055005050500055000000000000000000
00000000000000005000505050000050005000005050505050505050555000500000505000500000005050005500505050005050500050000000000000000000
00000000000000005000505055505500550000005050505050505500555055000000505055000000550055500550055055505050055055500000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000

__gff__
0004000000000000030700000000000000040004010501050105010500000005000400040501010501050105000000010105000405010105010501050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__map__
0000000000000000000000000000000000161616161616161616161616161600001a1a1a1a1a1a1a1a1a1a1a1a1a1a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018323232222222202022222232323214182222323232122222323232122222140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018323232222222202022222232323214182222222232322222222232322222140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018222222121212222212121222222214182010222232322010222232322010140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018222222121212222212121222222214182022221032322022221032322022140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018222222121212222212121222222214182222222020222222222020222222140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018323232222222202022222232323214182212323232222212323232222212140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018323232222222202022222232323214182222323232122222323232122222140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018222222121212222212121222222214182222222232322222222232322222140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018222222121212222212121222222214182010222232322010222232322010140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018222222121212222212121222222214182022221032322022221032322022140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018323232222222202022222232323214182222222020222222222020222222140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000018323232222222202022222232323214182212323232222212323232222212140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000010000001a1a1a1a1a1a1a1a1a1a1a1a1a1a01001616161616161616161616161616010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
010100030c5500c070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0101003f306053060500605006050c2131160500605006050060527605006050060500605006050060500605306050060500605006051a6050060518313126050060500605006052b61500605006050060500605
010200373060530605095231160500605006140c615276052b615006052d516006050060400605306050060500605006051a605006040060500000000001f400126051f40300605006052b605006050060500605
010100020c07018770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
010100020c7700c770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
013400200c1400c1310c1210d1310c1410c1310c1210d1310c1410c1310c1210d1310c1410c1310c1210d1310c1410c1310c1210d1310c1410c1310c1210d1310c1410c1310c1210d1310c1410c1310c1210d131
0104000c0c737187460c737187460c737187360c73718736000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007
010102030c5400c530001600010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100
017400000c8740c855148741485508b7408b550cb740cb550c8740c8550a8740a85508b7408b550cb740cb550c8740c855148741485508b7408b550cb740cb551d8541d8450a8740a85508b7408b550cb740cb55
0110002000a5618a4600a36189460095611a46009362194600a5600a461493600a460b9560094610a36009460c95600a461ba361294600956009460f936009461a95600a4600a360894600956169460093621a46
0114002007a161fa2607a261f9160791618a36079262891607a2607a361b92607a16129160792617a26079161391607a3622a2619916079260793616936079162192607a3607a360f926079161d9260793628a26
017400000c8740c855148741485508b7408b550cb740cb550c8740c87514c3414c3508b6408c650cb440c8650c8740c855148741485508b7408b550cb740cb550c8740c87514c6408c6508b6408c650cb440c865
0174000006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d7006d70
017400000c8740c855148741485508b7408b550cb740cb550c8740c8550a8740a85508b7408b550cb740cb550c8740c855148741485508b7408b550cb740cb551d8741d8550a8740a85508b7408b550cb740cb55
017400001572015015147201401517722170151572015015197201901518720180151a7201a01519720190151302013515127201201511720110151272012015130201351512720120150d0200d5151272212015
0174000002e0021e2021e1520e201471223e2223e1521e2021e1525e2025e1524e2024e1526e200e51525e2025e151fe20137151ee20125151de201de151ee20127151fe20137121ee201ee1519e200d5151ee22
0114002037a163791637a163f9062b9062ba062b9162ba162400024000240002ba162b9102ba162ba06369062b9063ba162b906379062da162d9102da062b9062b9063a9062b9163f9062ba062ba162b9162ba10
0174000004d7004d6104d5104d4104d5104d6104d7104d6104d5104d4104d5104d6104d7104d6104d5104d4104d5104d6104d7104d6104d5104d4104d5104d6104d7104d6104d5104d4104d5104d6104d7104d61
0168002037a163791637a161ce25105152ba062b9162ba16240001ce25240002ba162b9102ba160e515369062b9063ba162b906105152da162d9102da062b9062b9063a9062b916115151ce252ba162b9162ba10
016c002024b1524814248152c8142c81520b1420b1524b1424b152481424815228142281520b1420b1524b1424b1524814248152c8142c81520b1420b1524b1424b153581435815228142281520b1420b1524b14
016c002024b1524814248152c8142c81520b1420b1524b1424b1524814248152cc142cc1520b1420c1524b142481524814248152c8142c81520b1420b1524b1424b1524814248152cc1420c1520b1420c1524b14
010400000c023000031002300003130230000311023000030e0230000300003130231102310023000030e023000030c023000030e023000031002300003110231302315023000030c0230c0230e0230000310023
0102000011631106310e6270b62509625076150461500600006000060000600006000060000600006000060000600006000060000600006000060000600006000060000600006000060000600000000000000000
0102000011144101440e1340b13409124071140411400104001040010400104001040010400104001040010400104001040010400104001040010400104001040010400104001040010400104001040010400104
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0110000013746187361b7261871613716187161b7161871613716187161b7061870624b242482524814248152bb242bb152bb142bb151eb241eb151eb141eb151a74616736127261a7161a71616716127171a717
0110000014746177360f7261471614716177160f7161771614716177160f7161771623b242382523814238152bb242bb152bb142bb1520b2420b1520b1420b1513746167360e7261371613716167160e71616716
011000000cf700cf700cf600cf600cf510cf500cf410cf400cf110cf110cf110cf150cf000cf0513f6013f4112f6012f5112f4112f3116f6016f6116f5116f5116f2116f2116f1116f150cf600cf510cf410cf35
0110000014f7014f7014f6014f6014f5014f5014f4014f4014f1114f1114f1114f1513f7013f6013f5013f4013f3013f2013f1013f1013f1013f1000000000000ef700ef700ef600ef600ef500ef500ef400ef40
011000000c05337a163791637a161ce25105152ba062b9160c0531b1121b1121b1121b1121b1121b1121b1120c05337a163791637a161ce25105152ba062b9160c05312112121121211212112121121211212112
011000001b00013017180171b0171801713017180171b0171b1201b1101b1101b1101b1101b1101b1101c0111b3021b3052b8042b80501104101011e8041e8051212012110121101211012110121101211011011
011000001b00013017180171b0171801713017180171b017201202011020110201102011020110201102101120105201052b1042b1052b1042b1011e1041e1051712017110171101711017110171101711016011
011000000c05337a163791637a161ce25105152ba062b9160c053201122011220112201122011220112201120c05337a163791637a161ce25105152ba062b9160c05317112171121711217112171121711217112
011000000cf700cf600cf600cf600cf510cf500cf500cf500cf410cf400cf400cf400cf410cf4513f6013f4112f6012f5112f4112f3116f6016f6116f5116f5113f7013f6113f6113f5113f4112f3111f3110f41
011000000c05337a163791637a161ce25105152ba062b9160c05337a163791637a161ce25105152ba062b9160c05337a163791637a161ce25105152ba062b9160c05337a163791637a161ce25105152ba062b916
0110000014f7014f7014f6014f6014f5014f5014f4014f4014f1114f1114f1114f1513f7013f6013f5013f4013f3013f2013f1013f1013f1013f1000000000000ff700ff600ff500ff400ff300ff200ff100ff10
001000001701714017170170f0171401714017170170f017171201711017110171101711017110171101711017115201052b1042b1052b1042b1011e1041e1050e1200e1100e1100e1100e1100e1100e1100e110
011000000c05337a163791637a161ce25105152ba062b9160c053171121711217112171121711217112171120c05337a163791637a161ce25105152ba062b9160c0530e1120e1120e1120e1120e1120e1120e112
001000001771714717177170f7171471714717177170f717141201411014110141101411014110141101411014115201052b1042b1052b1042b1011e1041e1051312013110131101311013110131101311013110
001000000c05337a163791637a161ce25105152ba062b9160c053141121411214112141121411214112141120c05337a163791637a161ce25105152ba062b9160c05313112131121311213112131121311213112
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0104000011203117030010000100000000d4030d70301100011000100009403097030210002100020000530305703031000310000000041030470303100031000300000000000000000000000000000000000000
0103000011103117040010000100000000d5030d70301100011000100009503097030210002100020000550305703031000310000000045030470303100031000300000000000000000000000000000000000000
0103000011273117130010000100000000d4530d71301100011000100009443097130210002100020000533305713031000310000000041330471303100031000300000000000000000000000000000000000000
0103000011173117140010000100000000d5530d71301100011000100009543097130210002100020000553305713031000310000000045330471303100031000300000000000000000000000000000000000000
0105000011573117140010000100000000c5530c71300100001000000007543077130010000100000000253302713001000010000100001000010000100000000000000000000000000000000000000000000000
010300000c6141861427614346143c6243f6313e6313d6313b63138631336312c6212762124621216211d6211b62119621166211562115621146211462113611126151161511615106140e6150c6140a61507614
0001000009610096000c6100d6001161012600146101560018610196001b6101e6002b6102e60032610346003c62039621336212e62127621216211a621136110b61106611046110361102611026110000000000
010100000c270302710000000000000000000000000000000000000000000000024024241000000000000000000003f6713f651206411b64114641116310f6310d63109631086310562105621036210261102611
01070000246310000015613204031e6002b6240700010613166031410314603121031260311503116030f5030d6030c0030b6030b0030a6030970308603077030560304703046030270301603002030000300003
0101000029673022732260314473066731c203196630045316603004531464312203126430034311603003430d6430c2030b633003330a6030052308623072030562300513046030051301613002030000300003
010200002667007643006341162300624006230e61400613006143e6003e6003e600326003260032600326000f60018600106001960018600216001960022600146001d600156001e600166001f6001760020600
000100003f2703f1503f1403f1303f1203f1103f1103f1103f1103f1103f1103f1153f1003f1003f1003f1000f10018100101001910018100211001910022100141001d100151001e100161001f1001710020100
010100000a73713537092371273708547111470774710547051570e757045570d157037670c56702177000770f50018100107001950018100210001950022100140001d500151001e000165001f1001700020500
00010000117371a537122371b737135471c147147471d547141571d757155571e157167671f56717167207670f57718177107771957718100210001950022100140001d500151001e000165001f1001700020500
010200000b25411353020730070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700
010200001115300100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100001000010000100
01030000187741d765187541d745187341d7250070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700007000070000700
010200001b55516545115350000500005000050000500005000050000500005000050000500005000050000500005000050000500005000050000500005000050000500005000050000500005000050000500005
0102000013555185451d5350050500505005050050500505005050050500505005050050500505005050050500505005050050500505005050050500505005050050500505005050050500505005050050500505
__music__
03 0c0e0f10
01 08115312
00 0b115212
00 08111312
02 0b111412
01 1c1e6125
00 1c1e6125
00 1d1f6225
00 1d266225
01 1c1e2120
00 1c242223
00 1d1f2728
02 1d1f292a

